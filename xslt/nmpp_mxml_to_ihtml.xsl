<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:exsl="http://exslt.org/common" xmlns:set="http://exslt.org/sets" exclude-result-prefixes="exsl set">

<!--
	/*************************************************************************
	 *
	 * Copyright (c) 2005+ MedHand International AB
	 * All rights reservered.
	 *
	 *************************************************************************/
	/*
	 * @(#)wabcpp_mxml_to_ihtml.xsl
	 *
   * DESCRIPTION:
   * ====================
   * Stylesheet for converting MedHand XML(MXML) to iHtml.
   *
   * OVERRIDES:
   * ====================
   *
	 * CHANGES:
   * ====================
	 */-->
	 
<xsl:import href="../../common/ihtml/xslt/mxml_to_ihtml.xsl"/>
<xsl:variable name="style" select="'normal-style'"/>     
<xsl:variable name="folder-max-size" select="0"/>

<!-- *******************************************************************
			BOOK SPECIFIC FORMATING
     ******************************************************************* -->
     
<!-- *******************************************************************
			Breadcrumbs
     ******************************************************************* -->
     
<xsl:template name="getSearchClassBreadcrumb">
	<xsl:choose>
	<xsl:when test="following-sibling::table">
		<xsl:value-of select="normalize-space(ancestor::container[parent::book]/title)"/>
	</xsl:when>
	<xsl:when test="starts-with(parent::container/@pres-type,'none')">
		<xsl:value-of select="normalize-space(parent::container/title)"/> - <xsl:value-of select="normalize-space(ancestor::container[parent::book]/title)"/>
	</xsl:when>
	<xsl:otherwise>
		<xsl:value-of select="normalize-space(ancestor::container[parent::book]/title)"/>
	</xsl:otherwise>
	</xsl:choose>
	
</xsl:template>

<xsl:template name="getSearchKeywordBreadcrumb">
	<xsl:value-of select="normalize-space(ancestor::container[parent::book]/title)"/>
</xsl:template>

<xsl:template name="getSectionBreadcrumb">
</xsl:template>

<xsl:template name="getTableBreadcrumb">
	<xsl:value-of select="normalize-space(ancestor::container[parent::book]/title)"/>
</xsl:template>

<!-- image breadcrumbs, used in html header and and describes enviroment for a image bookmark&history entry  -->  
<xsl:template name="getImageBreadcrumb">
	<xsl:value-of select="normalize-space(ancestor::container[parent::book]/title)"/>
</xsl:template>
<xsl:variable name="link-video-repo" select="''"/>
<xsl:template name="getVideoBreadcrumb">
</xsl:template>

</xsl:stylesheet>
